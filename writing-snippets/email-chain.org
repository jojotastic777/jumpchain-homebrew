#+TITLE: Email Chain
#+AUTHOR: jojotastic777

* Summary
A short snippet of story, written during my most recent bout of thinking about Jumpchain. Consists mostly of a chain of emails, with a single IRC section and some more conventional narration mixed in.

Generally inspired by too many chatfics, too much screwing around with IRC servers over the last couple of days, having decided to tinker with Jumpchain again, and wondering what exactly the "Long Contract Jumpers" mentioned in the title of Pionoplayer's [[https://drive.google.com/file/d/1Sq4xCEqOPfGHzG-wbBk7kYfyrQMn_QS1/view][​Jumpchan's Premium Scaling Body Mod for Long Contract Jumpers]] are supposed to be.

This is a story in which an individual learns about their Jumpchain Adventure^TM, and gets to learn what exactly a Long Contract Jumper is supposed to be.

This is, as far as such things go, a complete story. I will not likely be writing more of this.

* Snippet
At 11:00 on a Saturday night, there is a small room lit dimly by the light of a trio of monitors.

A man is sitting in a cheap office chair, quietly contemplating the idea of writing a story.

An email client emits a bright and cheerful "Ding!"

----------

#+begin_src mail
From: rand-o@barstewards.agency
To: jojotastic777@star.forge
Subject: Congratulations!

You have been selected to recieve the opportunity
participate in a Jumpchain Adventure. Please reply within
the next 24 hours if you wish to participate in this
opportunity.
#+end_src

#+begin_src mail
From: jojotastic777@star.forge
To: rand-o@barstewards.agency
Subject: Re: Congratulations!

If this is indeed an actual offer, and not just some weird
joke, I am definitely interested.

Also, I've heard tell, in some places, of some kind of
"Extended Contract" or "Long Term Contract" or something
which provides additional benefits? Presumably in exchange
for something, though nobody has actually said WHAT. I
would be interested in hearing about such a thing, if it
exists.

After all, it is only reasonable to try and make the best
out of this opportunity.
#+end_src

#+begin_src mail
From: rand-o@barstewards.agency
To: jojotastic777@star.forge
Subject: Long Term Jumpchain Contract

I'm glad to hear you're interested, both in the Jumpchain
and in our new Long Term Jumpchain Contract.

While we don't normally tell most prospective Jumpers the
Long Term Jumpchain Contract, owing to it's relative
newness, your background check indicates that you would be
a FANTASTIC candidate for the program!

The Long Term Jumpchain Contract is a relatively new
initiative designed to combat a little known issue that our
Benefactors commonly face.

Did you know that over 80% of all Jumpers either fail their
Chain or choose to retire without Sparking Out after fewer
than 6 Jumps? And in 95% of THOSE cases, it's the Jumper's
own fault!

Sometimes the Jumper will grow overconfident after a string
of fairly easy Jumps, and get themselves killed going
somewhere deadlier than they're ready for. Other Jumpers
will run into something horribly traumatizing before they
get a Perk rendering them immune to that sort of thing, and
decide to retire.

Hell, SOME Jumpers end up deciding they miss their friends
and family (either from their original world or from a
Jump) and decide to go back. You'd think that's what the
Companion system would be for, and we even provide options
for pulling in people (and pets and items) from your
original universe as Companions with the Personal Reality
Supplement which has been standard for CENTURIES now, but
some people are just STUPID or something.

... Sorry, got a little heated there.

Anyways, the Long Term Jumpchain Contract. It's intended to
try and severely reduce the number of Jumpers who bail
early on or, failing that, ensure a smaller core of Jumpers
willing to stay on longer than a handful of Jumps.

The way it works is that, basically, if you want to be
eligable for a Long Term Jumpchain Contract, you pick a
proof-of-intent trial from a prepared list and agree to
undertake a certain number of Jumps up front. Then, after
that's settled, complete your trial at the very start of
your chain, without the benefit of any sort of Warehouse or
Body Mod or what have you. If you fail the trial, you get
sent home with nothing, your memories of the ordeal get
wiped, and we go looking for a new candidate. If you
succeed, you get to properly start your Chain with a
handful of benefits that normal Jumpers don't have access
to.

If you fail to undergo the agreed-upon number of Jumps due
to Chain-failure or otherwise being rendered unable to
continue your Chain, there are a number of ways you're
allowed to resolve the situation.

First, you could make Chain-failure impossible with
something like cliffc999's "Life Beyond Death", if you've
heard of that. This is generally the option we prefer, as
we can nudge circumstances while you're in the process of
reincarnating to try and ensure we don't need to do so
again.

Second, you can give up on a particular Jump if you die (or
otherwise "Chainfail") too many times. Generally, that
means that you exit the Jump and surrender all your
purchaces (and anything else you might have picked up) from
the Jump in question. It's sort of like failing a Gauntlet,
you surrender your goodies and move on the the next Jump.

Finally, if we feel that it is ABSOLUTELY NECESSARY, we CAN
just cut our losses and send you home with your memories of
the Chain wiped and your Perks and other benefits stripped.
We've only ever had to do that TWICE in the history of the
program, though, and we REALLY don't want to have to make
that number go up.

Of course, it goes without saying that the [Stay Here] and
[Go Home] options available to normal Jumpers aren't
something that Long Term Jumpchain Contractees can make use
of until they've fulfilled their side of the agreement.

In exchange for those stipulations, though, the benefits
you recieve are (in my opinion, at least) pretty good.
Screwing yourself over for great profit is one of the core
tenets of the Jumpchain, after all.

The most obvious benefit, of couse, is our Premium Scaling
Body Mod. While anyone who knows to ask can get access to
the PSBM at the "Peak Human" tier, Long Term Jumpchain
Contractees get access to the Good Stuff, and their PSBM is
set to the "Metahuman" tier.

Some of the other benefits of the Long Term Jumpchain
Contract include:
- Access to cliffc999's "Mail Order" system, which was
previously only available to Jumpers lucky enough to be
enrolled in our Beta program.
- Easy access to Chain-failure prevention mechanisms which
our Benefactors were previously more hesitant to hand out.
- Increased leeway when it comes to assorted bits of
rule-bending.
  - For example, a LTJC Jumper might be able to get away with
  using cliffc999's "Fiat-Loss Instead of Power-Loss" and
  taking cybernetics from the Eclipse Phase Jump into certain
  Gauntlets or a Jump with a Power-Loss Drawback enabled,
  where a normal Jumper wouldn't.
- The ability to petition your Benefactor for amusing bits
of rule-bending and rule-breaking on the fly.
  - An example of this would be changing the start time of a
  Jump by a few days in order to appear in the middle of a
  specific event, or to avoid something unpleasant that would
  happen at the very start of the Jump.

After your contract expires, you have two options for what
you can do:

If you feel like you want to continue Jumping for a long
time in the future, you can renew your contract for half
your initial promised number of Jumps in addition to the
Jumps you've already done.

If you choose to renew your contract, you obviously get to
keep all the normal benefits that come with it. In addition
to that, however, you gain access to a single "Bonus Jump"
per renewal, which is basically like cliffc999's "Modified
Front-Load", but in the middle of your chain and it doesn't
replace your Body Mod. (It obviously doesn't include the
Perks of that Jump into your Body Mod, either.)

If, however, you feel like you're getting tired of the
Jumpchain and you want to retire soon, you can either
choose to go [Go Home] or [Stay Here] at the end of the
last Jump on your contract, or you can downgrade to being a
Standard Jumper. As a bonus for having been part of the
LTJC Program, you even get to keep the extra PSBM access in
Gauntlets and Power-Loss situations, as well as whatever
Chain-failure protections you chose to employ.

If you change your mind after you downgrade, you can of
course come back to the Program at the start of your next
Jump. You won't even have to undergo a new proof-of-intent
trial! (You still have to sign up for the normal minimum
number of Jumps, however.)

So how about it? Have I sold you on the Long Term Jumpchain
Contract?
#+end_src

#+begin_src mail
From: jojotastic777@star.forge
To: rand-o@barstewards.agency
Subject: Re: Long Term Jumpchain Contract

Honestly, that all sounds pretty good. I only really have
three questions I want to ask before I sign up:

First, what is the minimum number of Jumps I'd have to sign
up for? I don't want to accidentally have to go for a
million Jumps or anything, and while I don't honestly think
that the minimum would be anywhere close to that, I'm
largely of the opinion that it's better to be safe than
sorry.

Second, what sorts of "proof-of-intent" tasks would I have
to choose from?

Finally, what are the rules for Sparking Out? I assume I'd
only allowed to try and Spark Out on the last Jump of the
Contract or after having downgraded or something, right?
#+end_src

#+begin_src mail
From: rand-o@barstewards.agency
To: jojotastic777@star.forge
Subject: Re: Re: Long Term Jumpchain Contract

Honestly, those are perfectly reasonable questions and I
wish more people would ask them before jumping in blindly.
Then maybe we would have fewer wash-outs from the program.

In regards to your first question, the answer kind of
varies? The LTJC is a fairly new program, so details like
that haven't been entirely ironed out yet. You're right,
though, that asking for a million Jumps would be ludicrous.
I don't even think we have a million Jump Documents in the
database, currently! You'd have to start using the "Generic
CYOA" document, and NOBODY wants that. No, since the
program started the biggest number the higher-ups have
tried was like, 50, but every one of those attempts washed
out of the program before they made it three quarters of
the way through.

More recently, the number has been hovering around 20,
which seems pretty reasonable. Right now the minimum is
pretty low, actually. 15.

As to your second question, while we normally try to keep
that a secret until you've already committed to signing up,
I asked my boss and he said your background check is good
enough that I should be fine to send you the list, so I've
attached it to this email.

When it comes to Sparking Out, our rules are actually
pretty generous about that, since almost nobody thinks to
try. You're allowed to take and End Jump/Scenario and try
to Spark Out any time in the latter half of your contract
(that is, after completing half of your required Jumps). If
you succeed, then your contract ends early with our
congratulations on your ascension, and if you fail then the
Jump ends immediately and doesn't count for the purposes of
fulfilling your contract.

Trying to Spark Out of the Chain is actually really
uncommon, and SUCCEEDING is actually less common than
someone naturally igniting their Spark. Something like one
in every trillion sentients in the omniverse COULD succeed
in Sparking Out, and our screening processes take that up
to about one in every ten billion Jumpers managing to Spark
Out, instead of choosing to [Go Home], [Stay Here],
Chainfailing, or even just continuing to Jump forever.

In the history of the Long Term Jumpchain Contract Program,
only ONE person has successfully managed to Spark Out, and
she very clearly went into her Chain with the intent to
Spark Out, and almost everything she did on her Chain was
to aid in that goal. A nice enough girl, I guess, but
INTENSE. My cousin was her Benefactor, actually. They still
keep in touch, apparently she likes to hear updates about
how the Program is going.

Anyways, please read through the list of tests and get back
to me in about a half hour regarding which of them you
think you'd be willing to do, okay? It'll probably take you
a bit, so I'm going to go take my coffee break.
#+end_src
Attached to the above email is a file, ~test-of-intent.pdf~. Inside the file are listed a wide variety of challenges suitable for testing the subject's patience and sense of delayed gratification.

Amongst those tasks is the following:
#+begin_quote
*Test of Automation* (ID: ~514937f8-b4a4-4503-8bb8-bfb77ef7120a~)
-----------------------------------------------------------------------------------------------------
At the very beginning of your Chain, before recieving your Body Mod, Warehouse, or other Chain-derived benefits, take the _Satisfactorio Gauntlet_.

You are required to take a "*Mission*" Drawback with a minimum of =+700cp=, and then achieve the "_Mission Complete_" Reward for the Gauntlet.

Bonus Rewards:
- Keep the Gauntlet Rewards
- =+200bp= via the Premium Scaling Body Mod's "*Delayed Gratification*" option.
#+end_quote

#+begin_src mail
From: jojotastic777@star.forge
To: rand-o@barstewards.agency
Subject: Sign Me Up

That all seems pretty reasonable, honestly.

Regarding my proof-of-intent I think that, given the
option, I'd probably go with the "Test of Automation"
(ID: 514937f8-b4a4-4503-8bb8-bfb77ef7120a).

I always did like Factorio, though I never did actually get
around to playing much of Satisfactory. I suppose now I'll
sort of get the chance.

(Oh, and in case it isn't obvious, I'd like to sign up for
15 Jumps then hopefully extend after I see how I feel about
the Chain.)
#+end_src

#+begin_src mail
From: rand-o@barstewards.agency
To: jojotastic777@star.forge
Subject: Re: Sign Me Up

Alright, sounds good. With that information put into the
database, all I need to do now is pause your universe and
pull you into your trial.

As a bonus for being so co-operative and barely expressing
any disbelief, I'll even pull you into the trial with
whatever you can carry on your person (with, like, a
backpack or something) and give you time to grab your
stuff.

Just reply to this email and let me know when you're ready.
You have 15 minutes.
#+end_src

"Shit. Okay. This is feeling kind of serious."

A moment passes.

"... It's probably a good thing I'm weird enough to make sure my normal 'every day carry' can double as an 'isekai bag'."

The squeaking of an overburdened office chair.

Footsteps. Opening a backpack.

Shuffling. A moment later, the backpack closes.

"All set."

#+begin_src
​*** jojotastic777 (~jojotastic777@star.forge) Joined #general
<jojotastic777> I've got some wierd shit going on where I am.
<jojotastic777> Emails to and from an address that doesn't exist, on a domain that VERIFIABLY doesn't exist.
<arglbargl> (⊙＿⊙')
<jojotastic777> ... Probably wouldn't be a good idea to get into much detail.
<@pepsiman> You do know you're sounding kind of crazy, right?
<jojotastic777> Yeah, probably. It's probably just some jackass doing some kind of weird roleplay joke thing.
<jojotastic777> Still, I'll probably be busy for a little while even so. Even if it IS probably a joke.
<arglbargl> :/
<TenMilKelvin> yeah yeah, have fun with your writing bullshit or whatever
<arglbargl> ┌П┐(ಠ_ಠ)
<TenMilKelvin> fuck off
<jojotastic777> ... Hey. Just in case, could you all do something for me?
<jojotastic777> If I don't come back in like a week, could someone go to https://failsafe.star.forge and hit my failsafe early?
<@pepsiman> Really? Based on some jokester sending you a couple of emails?
<arglbargl> (ㆆ _ ㆆ)
<jojotastic777> I dunno, I just have a wierd feeling.
<jojotastic777> I'm setting the failsafe password for 2023-04-29 (PST) to "hunter2". For the meme, you understand.
<jojotastic777> If it turns out to be nothing, I'll just lock out the failsafe on that day.
<@pepsiman> Ok, fine. If you're really worried.
<TenMilKelvin> Fuck, okay. Fine.
<arglbargl> ... ok
<jojotastic777> Thank you. Hopefully I'll be back soon.
​*** jojotastic777 (~jojotastic777@star.forge) Quit: "I have a bad feeling about this..."
<TenMilKelvin> wait. WHAT THE FUCK. DID ARGLBARGL JUST SPEAK NORMALLY?
<@pepsiman> Seriously, Kelivn? THAT is what you want to comment on?
<arglbargl> (✿◠‿◠)
​* pepsiman sighs
<@pepsiman> why am i the only sane person here
<arglbargl> ¯\_(ツ)_/¯
#+end_src

#+begin_src mail
From: jojotastic777@star.forge
To: rand-o@barstewards.agency
Subject: Ready To Go

Honestly, I'm really glad I made a hobby out of preparing
for this sort of thing. I'm ready whenever you are.
#+end_src

#+begin_src mail
From: rand-o@barstewards.agency
To: jojotastic777@star.forge
Subject: Re: Ready To Go

Really dude? Warning your IRC channel? Whatever, SUPER not
my problem. Not like they're likely to be able to do
anything before you're not our problem anymore.

I'm pulling you out in 10 seconds. You might want to be
standing up, unless you want to fall on your butt.
#+end_src

"Shit."

----------

At 12:57 on an early Sunday morning, there is small room lit dimly by the light of a trio of monitors.

A cheap office chair spins slowly on it's swivel, no longer burdened with the weight of it's occupant.

On the central monitor, there is a terminal open.

#+begin_src
[jojotastic777@mastercom-starforge]$ sudo shutdown now
[sudo] password for jojotastic777:
#+end_src

A cursor blinks cheerfully at the end of a line, waiting for a password that will never come.

* Author's Notes
Really, I didn't go into this intending to write anything more than a couple of paragraphs of flavor text for my latest bout of trying to build a Jumpchain. I certainly didn't expect whatever the heck /this/ is supposed to be.

I /definitely/ didn't expect to end up writing an /email chain,/ of all things. What a ride, man.

Regardless, I'm actually pretty pleased with how this came out, for my first semi-serious attempt at creative writing in the last couple of /years./ I'm /especially/ happy with the IRC bit, and the symmetry between the beginning and the ending. "there is a small room lit dimly by the light of a trio of monitors", and all that.

Also, "A cursor blinks cheerfully at the end of a line, waiting for a password that will never come." is possibly the most emotionally charged sentence I have ever written.
