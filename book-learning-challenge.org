#+TITLE: jojotastic777's 'Book Learning' Challenge
#+AUTHOR: jojotastic777

When a Perk would directly give you knowledge and/or skills, you are instead provided with sufficient instructional materials of exceptional quality to learn the knowledge and/or skills in question. Usually, this takes the form of one or more books. Alternative instructional media will be provided in situations where appropriate for the knowledge in question, and in situations where the Jumper would be /unable/ to read a book due to Drawback Fiat, whether due to being physically blinded, rendered completely illiterate, or otherwise rendered unable to read.

If you already possess an information storage Item (E.g. a transforming book that can become any book that you own, or a phone/tablet/laptop/e-reader with "infinite storage"), you may (at your disgression) choose to have the provided instructional materials "added" to the Item in question in whatever manner is appropriate for the Item in question.

You are guaranteed to be able to learn whatever knowledge and/or skills in question by thoroughly going through the instructional materials and practice, even for knowledge and/or skills that would normally require a teacher. This benefit is only provided to you. For everyone else, the provided instructional materials are perfectly ordinary, if perhaps of unusually high-quality.

To be absolutely clear, this doesn't interfere with Perks that improve your ability to learn, Perks that improve your ability to find teachers, Perks that give you a /talent,/ or /literally anything/ other than those Perks which directly put knowledge and/or skills into your mind.

The effects of this Challenge don't impact memories provided to you as part of non-Drop-In Backgrounds/Origins. Those memories /may,/ at your Benefactor's disgression, include having picked up /some/ or /all/ of the knowledge and/or skills provided by your Perks, provided that the knowledge and/or skills in question are something that an individual with your background could be expected to know.
